#!/bin/bash

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

# © Miguel Anxo Bouzada <mbouzada[fix@]gmail[fix.]com>
# © Proxecto Trasno <proxecto[fix@]trasno[fix.]net>
# july 2012

## -

# execute « [bash |./]xera [prepareDir|php2po|po2php|po2tmx|po-old] »

## Asignación de variábeis de nomes de directorios de traballo
## Axuste estes nomes de cartafoles á súa comenencia

## Assigning directory names variables for  work
## Set these directory names for your convenience

PHP_Original="en"       # No caso de exemplo o « en » de Moodle
PHP_Target="gl"         # No caso de exemplo o « gl » que imos crear

Old_PHP="gl"            # No caso de exemplo o « gl antigo » de Moodle
Old_PO="po"             # No caso de exemplo o « po » que se vai xerar

PO_Original="po-orixe"
PO_Target="po-destino"
PO_Accumulated="po-acumulado"  	
PO_Work="po-traballo"   
PO_Final="po-final"     

tmp4TMX="tmp4tmx"    
TMX="tmx"
tmp4accumulated="tmp4acumulado"               

function prepareDir(){
    # Esta función descomprime os ficheiros descargados,
    # xera os cartafoles/directorios precisos e coloca os antigos no seu sitio

    # This function unzip downloaded archives,
    # Creates the required folders/dirs and puts the old in place

    [ -d ${PHP_Original} ] && echo "SEMELLA QUE ESTA FUNCIÓN XA FOI EXECUTADA" && exit 0

    mkdir OLD ${PO_Work} ${PO_Final}

    unzip ${PHP_Original}.zip

    unzip  ${Old_PHP}.zip -d OLD/

	mv ${Old_PHP}.zip OLD_${Old_PHP}.zip
}

function phpTOpo(){
    # Esta función fai conversións masivas de PHP a PO
    # This function makes massive conversions of PHP to PO

    # listamos o directorio "${PHP_Original}" xa que interésanos todo o que hai nel
    ls ./${PHP_Original} > list.txt

    [ -d ${PO_Original} ] && rm ${PO_Original}/* || mkdir ${PO_Original}

    while read NAME
    do
        PONAME=`echo $NAME | cut -d. -f1`
        php2po ${PHP_Original}/$NAME ${PO_Original}/$PONAME.po
    done < list.txt

    rm list.txt
}

function totalTOpo(){
    # Esta función fai conversións masivas de PHP a PO considerando as traducións xa feitas
    # This function makes massive conversions of PHP to PO considering the translations already made

    # listamos o directorio "OLD/${PHP_Target}" xa que só nos interesa o que hai nel (teña traducións previas)
    ls OLD/${PHP_Target} > list.txt

    [ -d ${PO_Accumulated} ] && rm ${PO_Accumulated}/* || mkdir ${PO_Accumulated}

    while read NAME
    do
        PONAME=`echo $NAME | cut -d. -f1`
        php2po -t ${PHP_Original}/$NAME OLD/${PHP_Target}/$NAME ${PO_Accumulated}/$PONAME.po
    done < list.txt

    rm list.txt
}

function poTOphp(){
    # Esta función fai conversións masivas de PO a PHP
    # e xera o arquivo comprimido .zip que emprega Moodle

    # This function makes massive conversions of PO to PHP
    # And creates the compressed .zip file it uses Moodle

    # listamos o directorio "${PO_Final}" xa que só nos interesa o que hai nel
    ls ./${PO_Final} > list.txt

    [ -d ${PHP_Target} ] && rm ${PHP_Target}/* || mkdir ${PHP_Target}

    while read NAME
    do
        PHPNAME=`echo $NAME | cut -d. -f1`
        po2php -t ${PHP_Original}/$PHPNAME.php ${PO_Final}/$NAME ${PHP_Target}/$PHPNAME.php
    done < list.txt

    [ -f ${PHP_Target}.zip ] && rm ${PHP_Target}.zip
    zip ${PHP_Target}.zip ${PHP_Target}/*

    rm list.txt
}

function poTOtmx(){
    # Esta función xera un "TMX" a partires de todo o traballo feito
    # This function generates a "TMX" from all the work done

    DATA=`date +%d%m%y-%H`

    [ -d ${tmp4TMX} ] && rm ${tmp4TMX}/* || mkdir ${tmp4TMX}
    [ -d ${TMX} ] && rm ${TMX}/* || mkdir ${TMX}

    # Facemos un po/tmx só dos ficheiros nos que se traballou nesta quenda
    # We make a po/tmx only of the files that were worked on this turn

    ls ${PO_Final} > currentList.txt

    msgcat -D ${PO_Final} -f currentList.txt -u -o ${TMX}/POCurrent_${DATA}.po
    po2tmx ${TMX}/POCurrent_${DATA}.po -l gl ${TMX}/TMXCurrent_${DATA}.tmx

    # Facemos un po/tmx de todos os ficheiros, incluídos os traducidos antigos, previamente filtrados
    # We make a po / tmx of all the files, including the old translated ones, previously filtered

    [ -d ${tmp4accumulated} ] && rm ${tmp4accumulated}/* || mkdir ${tmp4accumulated}
    cp ${PO_Accumulated}/* ${tmp4accumulated}/
    ls ${PO_Final} > listOfFinals.txt

    while read FILE
    do
        rm ${tmp4accumulated}/$FILE
    done < listOfFinals.txt

    cp ${PO_Work}/* ${tmp4TMX}/
    cp ${PO_Final}/* ${tmp4TMX}/
    cp ${tmp4accumulated}/* ${tmp4TMX}/

    ls ${tmp4TMX} > totalList.txt

    msgcat -D ${tmp4TMX} -f totalList.txt -u -o ${TMX}/POTotal_${DATA}.po
    po2tmx ${TMX}/POTotal_${DATA}.po -l gl ${TMX}/TMXTotal_${DATA}.tmx

    rm currentList.txt listOfFinals.txt totalList.txt 
	rm -r ${tmp4TMX} ${tmp4accumulated}
}

function po-old(){
    # Esta función fai conversións masivas de PHP a PO
    # Neste caso está elaborado para facer PO de ficheiros
    # PHP xa traducidos, ou con partes traducidas,
    # e xa postos, xeramos un PO Total e un oldTMX.

    # This function makes massive conversions of PHP to PO
    # In this case is made to PO files
    # PHP already translated, or translated parts,
    # And now stands, we generate a Total PO and oldTMX.


    # listamos só o directorio "OLD/${Old_PHP}" xa que só nos interesa o que haia nel
    ls OLD/${Old_PHP} > lista.txt

    [ -d OLD/${Old_PO} ] && rm OLD/${Old_PO}/* || mkdir OLD/${Old_PO}

    while read NAME
    do
        PONAME=`echo $NAME | cut -d. -f1`
        php2po -t ${PHP_Original}/$NAME OLD/${Old_PHP}/$NAME OLD/${Old_PO}/$PONAME.po
    done < lista.txt

    [ -d OLD/ALL ] && rm OLD/ALL/* || mkdir OLD/ALL

    ls OLD/${Old_PO} > list.txt

    msgcat -D OLD/${Old_PO} -f lista.txt -u -o OLD/ALL/ALL.po
    po2tmx OLD/ALL/ALL.po -l gl OLD/ALL/oldTMX.tmx

    rm list.txt
}

param=$1
[ $param = "" ] && exit 0
[ $param = prepareDir ] && prepareDir
[ $param = php2po ] && phpTOpo
[ $param = total2po ] && totalTOpo
[ $param = po2php ] && poTOphp
[ $param = po2tmx ] && poTOtmx
[ $param = po-old ] && po-old

#.EOF
