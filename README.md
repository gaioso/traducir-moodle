# Consellos para traducir o Moodle

> Este documento foi desenvolvido por [Miguel Anxo Bouzada](mailto:mbouzada@gmail.com) por encargo da [Oficina de Software Libre do CIXUG](https://osl.cixug.es), e adaptado ao formato Gitbook por [Rafael Rodríguez](mailto:rrgaioso@gmail.com).

> Este documento publícase baixo a licenza [Creative Commons Atribución–Compartir igual(CC BY–SA) 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/legalcode).

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Contidos

- [Descrición xeral e características comúns a calquera fase do proceso](#descrición-xeral-e-características-comúns-a-calquera-fase-do-proceso)
- [Obtención dun ficheiro traducíbel «gettext» .po a partires dun ficheiro .php](#obtención-dun-ficheiro-traducíbel-«gettext»-po-a-partires-dun-ficheiro-php)
    - [Descarga dos arquivos/ficheiros de traballo](#descarga-dos-arquivosficheiros-de-traballo)
    - [Traballar con ficheiros sen ningunha tradución cara ao idioma destino](#traballar-con-ficheiros-sen-ningunha-tradución-cara-ao-idioma-destino)
    - [Traballar con ficheiros parcialmente traducidos no idioma destino](#traballar-con-ficheiros-parcialmente-traducidos-no-idioma-destino)
    - [Conversión dun ficheiro «gettext» .po traducido a outro .php específico do idioma destino](#conversión-dun-ficheiro-«gettext»-po-traducido-a-outro-php-específico-do-idioma-destino)
- [Caso Moodle, procesamento masivo](#caso-moodle-procesamento-masivo)
    - [Procedemento base](#procedemento-base)
    - [Dinámica de traballo](#dinámica-de-traballo)
- [Script xera](#script-xera)
- [Problemas frecuentes](#problemas-frecuentes)
    - [Cadeas novas con tradución antiga](#cadeas-novas-con-tradución-antiga)
    - [Ficheiros truncados](#ficheiros-truncados)
    - [Solucións](#solucións)
        - [Para os ficheiros truncados](#para-os-ficheiros-truncados)
        - [Para as cadeas truncadas](#para-as-cadeas-truncadas)
    - [Cambios no contido das cadeas](#cambios-no-contido-das-cadeas)
    - [Solución:](#solución)
- [Script compLines](#script-complines)
- [Terminoloxía e estilo](#terminoloxía-e-estilo)
    - [Construción sinxela dun ficheiro de terminoloxía «TBX», a partires dunha folla en «calc»](#construción-sinxela-dun-ficheiro-de-terminoloxía-«tbx»-a-partires-dunha-folla-en-«calc»)
        - [Exemplo con odstotbx](#exemplo-con-odstotbx)
    - [Instalación](#instalación)
    - [Exportar](#exportar)
    - [Notas sobre o ficheiro resultante](#notas-sobre-o-ficheiro-resultante)
    - [Cambiar a cabeceira do TBX](#cambiar-a-cabeceira-do-tbx)
- [Bibliotecas útiles](#bibliotecas-útiles)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Descrición xeral e características comúns a calquera fase do proceso

Debemos ter en conta a estrutura dos ficheiros de idioma de PHP, tomamos como exemplo a primeira cadea de ``admin.php``

* En inglés:

	$string['accessdenied'] = 'Access denied';
	
* E en galego:

	$string['accessdenied'] = 'Acceso denegado';

Onde podemos ver que a estrutura é:

	$string['id_da_cadea'] = 'Cadea de texto';

O ficheiro .po de gettext debe ter esta estrutura:

	#: $string['accessdenied']
	msgid "Access denied"
	msgstr "Acceso denegado"
	
Polo tanto, ao converter un ficheiro .php cara .po, necesitamos asignar a cadea de texto en inglés á liña «msgid» e a cadea en galego á liña «msgstr», deixando o id_da_cadea (completo) como comentario para o tradutor «#:»

No caso de non existir tradución, no ficheiro .po veriamos o seguinte:

	#: $string['accessdenied']
	msgid "Access denied"
	msgstr ""
	
Polo que, a conversión php cara po obriga a empregar sempre o ficheiro en inglés como modelo (parámetro -t). Á inversa (po cara php) ocorre o mesmo, temos que empregar o ficheiro en inglés como modelo.

## Obtención dun ficheiro traducíbel «gettext» .po a partires dun ficheiro .php

### Descarga dos arquivos/ficheiros de traballo

No noso caso: https://download.moodle.org/langpack/3.1/ (ollo co número de versión).

> Nota: Descargar o ficheiro orixe «en» e o destino «gl».

### Traballar con ficheiros sen ningunha tradución cara ao idioma destino

Se non temos ningún ficheiro .php en galego, só precisamos partir do ficheiro orixinal en inglés:

	php2po /ruta/en/nome_ficheiro.php /ruta/??/c.po

### Traballar con ficheiros parcialmente traducidos no idioma destino

Se xa temos un ficheiro .php en galego, entón precisamos empregar os dous ficheiros (en e gl) como modelos:

	php2po -t /ruta/en/nome_ficheiro.php /ruta/gl/nome_ficheiro.php nome_ficheiro.po
	
Máis detalles na páxina de [Translate Toolkit](http://translate-toolkit.readthedocs.org/en/latest/commands/php2po.html)

### Conversión dun ficheiro «gettext» .po traducido a outro .php específico do idioma destino

	po2php -t /ruta/en/nome_ficheiro .php /ruta/??/nome_ficheiro .po /ruta/gl/nome_ficheiro .php
	
## Caso Moodle, procesamento masivo

> Todo o proceso indicado aquí depende das ferramentas do Translate Toolkit, máis concretamente da ferramenta [php2po](http://translate.sourceforge.net/wiki/toolkit/php2po).

### Procedemento base

O primeiro paso e crear un cartafol de traballo que aquí chamaremos PHP-PO.

Deseguido descargamos os ficheiros que imos empregar.

Na [Páxina de descargas do Moodle](http://download.moodle.org/langpack/) atopamos a versión 3.1, nesta [ligazón](http://download.moodle.org/langpack/3.1/) buscamos a versión en inglés (considerada a orixinal ou texto matriz) e atopamos un paquete zip co nome [en.zip](http://download.moodle.org/download.php/langpack/3.1/en.zip). Buscamos tamén a versión en galego e atopamos un paquete chamado [gl.zip](http://download.moodle.org/download.php/langpack/3.1/gl.zip)
. 
Descargamos ambos e, neste mesmo cartafol engadimos o script que vai na fin desta páxina, gardámolo co nome **xera** e dámoslle permisos de execución, co que teremos un cartafol máis ou menos así: 

![image](/imaxes/cartafol_inicial.png)

### Dinámica de traballo

1. Preparar a estrutura de traballo executando a orde ``./xera prepareDir``. O script descomprimirá os ZIP onde corresponda e xerará os cartafoles de traballo. 
2. Converter todos os PHP orixinais (inglés) a PO executando a orde ``./xera php2po``. O script xerará un cartafol chamado *po-orixinal*, no que teremos os ficheiros PO listos para traducir.
3. Converter os PHP destino (galego) e os PHP orixinais (inglés) a PO que conteñan a tradución xa feita executando a orde ``./xera total2po``. O script xerará un cartafol chamado po-acumulado, no que teremos os ficheiros PO listos para revisar si están ao 100% ou para completar a tradución. É importante salientar que neste cartafol só estarán aqueles ficheiros que teñan algunha cadea traducida.
4. Copiar o ficheiro que queiramos traducir cara *po-traballo*.
5. Traducir e revisar.
6. A seguir, unha vez conformes coa tradución, enviamos manualmente o ficheiro traducido ao cartafol *po-final*.
7. Aínda que non teñamos nada no cartafol *po-final*, poderemos xerar un ficheiro *tmx* para compartir o traballo que levamos feito con outro(s) tradutor(es) empregando a orde ``./xera po2tmx`` que xerará un ficheiro, coa data e hora actual no cartafol *tmx*. Xerará tamén un único PO. 
8. Como fase final, deberemos xerar os ficheiros PHP en galego, para remitírllelos ao(s) desenvolvedor(es). Empregaremos a orde ``./xera po2php``, con isto teremos un cartafol chamado *gl-php* e un arquivo *gl.zip*.

![image](/imaxes/cartafol_preparedir.png)

Executaremos:

	./xera [prepareDir|php2po|po2php|po2tmx|po-old]
	
Explicamos os parámetros: 

* *prepareDir* - Esta función fai os preparativos iniciais, descomprimir os .zip e crear algún dos directorios de traballo. 
* *php2po* - Esta función fai conversións masivas de PHP a PO. 
* *Total2po* - Esta función fai conversións masivas de PHP a PO  considerando as traducións xa feitas. 
* *po2php* - Esta función fai conversións masivas de PO a PHP e xera o arquivo comprimido .zip que emprega Moodle. 
* *po2tmx* - Esta función xera un tmx a partires de todo o traballo feito. 
* *po-old* - Esta función fai conversións masivas de PHP a PO. Neste caso está elaborado para facer ficheiros PO a partires de ficheiros PHP xa traducidos, ou con partes traducidas.

![image](/imaxes/cartafol_scripts_executados.png)

![image](/imaxes/cartafol_old.png)

## Script xera

[Descargar](/scripts/xera)

```
#!/bin/bash

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

# © Miguel Anxo Bouzada <mbouzada[fix@]gmail[fix.]com>
# © Proxecto Trasno <proxecto[fix@]trasno[fix.]net>
# july 2012

## -

# execute « [bash |./]xera [prepareDir|php2po|po2php|po2tmx|po-old] »

## Asignación de variábeis de nomes de directorios de traballo
## Axuste estes nomes de cartafoles á súa comenencia

## Assigning directory names variables for  work
## Set these directory names for your convenience

PHP_Original="en"       # No caso de exemplo o « en » de Moodle
PHP_Target="gl"         # No caso de exemplo o « gl » que imos crear

Old_PHP="gl"            # No caso de exemplo o « gl antigo » de Moodle
Old_PO="po"             # No caso de exemplo o « po » que se vai xerar

PO_Original="po-orixe"
PO_Target="po-destino"
PO_Accumulated="po-acumulado"  	
PO_Work="po-traballo"   
PO_Final="po-final"     

tmp4TMX="tmp4tmx"    
TMX="tmx"
tmp4accumulated="tmp4acumulado"               

function prepareDir(){
    # Esta función descomprime os ficheiros descargados,
    # xera os cartafoles/directorios precisos e coloca os antigos no seu sitio

    # This function unzip downloaded archives,
    # Creates the required folders/dirs and puts the old in place

    [ -d ${PHP_Original} ] && echo "SEMELLA QUE ESTA FUNCIÓN XA FOI EXECUTADA" && exit 0

    mkdir OLD ${PO_Work} ${PO_Final}

    unzip ${PHP_Original}.zip

    unzip  ${Old_PHP}.zip -d OLD/

	mv ${Old_PHP}.zip OLD_${Old_PHP}.zip
}

function phpTOpo(){
    # Esta función fai conversións masivas de PHP a PO
    # This function makes massive conversions of PHP to PO

    # listamos o directorio "${PHP_Original}" xa que interésanos todo o que hai nel
    ls ./${PHP_Original} > list.txt

    [ -d ${PO_Original} ] && rm ${PO_Original}/* || mkdir ${PO_Original}

    while read NAME
    do
        PONAME=`echo $NAME | cut -d. -f1`
        php2po ${PHP_Original}/$NAME ${PO_Original}/$PONAME.po
    done < list.txt

    rm list.txt
}

function totalTOpo(){
    # Esta función fai conversións masivas de PHP a PO considerando as traducións xa feitas
    # This function makes massive conversions of PHP to PO considering the translations already made

    # listamos o directorio "OLD/${PHP_Target}" xa que só nos interesa o que hai nel (teña traducións previas)
    ls OLD/${PHP_Target} > list.txt

    [ -d ${PO_Accumulated} ] && rm ${PO_Accumulated}/* || mkdir ${PO_Accumulated}

    while read NAME
    do
        PONAME=`echo $NAME | cut -d. -f1`
        php2po -t ${PHP_Original}/$NAME OLD/${PHP_Target}/$NAME ${PO_Accumulated}/$PONAME.po
    done < list.txt

    rm list.txt
}

function poTOphp(){
    # Esta función fai conversións masivas de PO a PHP
    # e xera o arquivo comprimido .zip que emprega Moodle

    # This function makes massive conversions of PO to PHP
    # And creates the compressed .zip file it uses Moodle

    # listamos o directorio "${PO_Final}" xa que só nos interesa o que hai nel
    ls ./${PO_Final} > list.txt

    [ -d ${PHP_Target} ] && rm ${PHP_Target}/* || mkdir ${PHP_Target}

    while read NAME
    do
        PHPNAME=`echo $NAME | cut -d. -f1`
        po2php -t ${PHP_Original}/$PHPNAME.php ${PO_Final}/$NAME ${PHP_Target}/$PHPNAME.php
    done < list.txt

    [ -f ${PHP_Target}.zip ] && rm ${PHP_Target}.zip
    zip ${PHP_Target}.zip ${PHP_Target}/*

    rm list.txt
}

function poTOtmx(){
    # Esta función xera un "TMX" a partires de todo o traballo feito
    # This function generates a "TMX" from all the work done

    DATA=`date +%d%m%y-%H`

    [ -d ${tmp4TMX} ] && rm ${tmp4TMX}/* || mkdir ${tmp4TMX}
    [ -d ${TMX} ] && rm ${TMX}/* || mkdir ${TMX}

    # Facemos un po/tmx só dos ficheiros nos que se traballou nesta quenda
    # We make a po/tmx only of the files that were worked on this turn

    ls ${PO_Final} > currentList.txt

    msgcat -D ${PO_Final} -f currentList.txt -u -o ${TMX}/POCurrent_${DATA}.po
    po2tmx ${TMX}/POCurrent_${DATA}.po -l gl ${TMX}/TMXCurrent_${DATA}.tmx

    # Facemos un po/tmx de todos os ficheiros, incluídos os traducidos antigos, previamente filtrados
    # We make a po / tmx of all the files, including the old translated ones, previously filtered

    [ -d ${tmp4accumulated} ] && rm ${tmp4accumulated}/* || mkdir ${tmp4accumulated}
    cp ${PO_Accumulated}/* ${tmp4accumulated}/
    ls ${PO_Final} > listOfFinals.txt

    while read FILE
    do
        rm ${tmp4accumulated}/$FILE
    done < listOfFinals.txt

    cp ${PO_Work}/* ${tmp4TMX}/
    cp ${PO_Final}/* ${tmp4TMX}/
    cp ${tmp4accumulated}/* ${tmp4TMX}/

    ls ${tmp4TMX} > totalList.txt

    msgcat -D ${tmp4TMX} -f totalList.txt -u -o ${TMX}/POTotal_${DATA}.po
    po2tmx ${TMX}/POTotal_${DATA}.po -l gl ${TMX}/TMXTotal_${DATA}.tmx

    rm currentList.txt listOfFinals.txt totalList.txt 
	rm -r ${tmp4TMX} ${tmp4accumulated}
}

function po-old(){
    # Esta función fai conversións masivas de PHP a PO
    # Neste caso está elaborado para facer PO de ficheiros
    # PHP xa traducidos, ou con partes traducidas,
    # e xa postos, xeramos un PO Total e un oldTMX.

    # This function makes massive conversions of PHP to PO
    # In this case is made to PO files
    # PHP already translated, or translated parts,
    # And now stands, we generate a Total PO and oldTMX.


    # listamos só o directorio "OLD/${Old_PHP}" xa que só nos interesa o que haia nel
    ls OLD/${Old_PHP} > lista.txt

    [ -d OLD/${Old_PO} ] && rm OLD/${Old_PO}/* || mkdir OLD/${Old_PO}

    while read NAME
    do
        PONAME=`echo $NAME | cut -d. -f1`
        php2po -t ${PHP_Original}/$NAME OLD/${Old_PHP}/$NAME OLD/${Old_PO}/$PONAME.po
    done < lista.txt

    [ -d OLD/ALL ] && rm OLD/ALL/* || mkdir OLD/ALL

    ls OLD/${Old_PO} > list.txt

    msgcat -D OLD/${Old_PO} -f lista.txt -u -o OLD/ALL/ALL.po
    po2tmx OLD/ALL/ALL.po -l gl OLD/ALL/oldTMX.tmx

    rm list.txt
}

param=$1
[ $param = "" ] && exit 0
[ $param = prepareDir ] && prepareDir
[ $param = php2po ] && phpTOpo
[ $param = total2po ] && totalTOpo
[ $param = po2php ] && poTOphp
[ $param = po2tmx ] && poTOtmx
[ $param = po-old ] && po-old

#.EOF
```

A información reflectida nesta sección está dispoñíbel tamén no [Wiki do GUL](http://wiki.galpon.org/Conversiones_masivas_PHP_-_PO_-_TMX) ao que pertence o autor desta documentación.

## Problemas frecuentes

### Cadeas novas con tradución antiga

Co método que seguimos, `(./xera po-old)` as cadeas *#fuzzy* non quedan debidamente marcadas xa que ao acumular a versión nova e a antiga, a unha cadea nova aplícalle a tradución da cadea antiga que teña o mesmo identificador. 

($string['advanced:anchor_desc'])

Este método, aínda que rudimentario, permite poder atopar erratas de traballos anteriores e así, aos poucos, ir mellorando os textos.

### Ficheiros truncados

Nalgunha ocasión temos atopado un problema nas conversións. Na conversión de .php cara .po, nun punto xerábase un EOF e o ficheiro resultante só contiña unha parte do total das cadeas. Sospeito que isto era debido a concorrencia (así foi nun caso) dos caracteres «';» no interior dunha cadea, e isto é o fin dunha cadea en PHP, polo que ao non seguir con «$string...» interpretaba que o ficheiro remataba aí. 
Noutros casos perdíase parte final dalgunha cadea msgid. Neste caso non fun quen de deducir ningunha causa probábel.
A fin de poder facer as comprobacións oportunas, xerei un script (compLines) que permite unha análise dos contidos, comparando as concorrencias de «$string» nos ficheiros orixe e destino e facendo unha conta de liñas.
A concorrencia de «$string[» debe ser total, e a de liñas ou igual ou moi próxima, calquera destes dous datos vai permitirnos fixarnos naqueles ficheiros que teñan discordancias e comprobar se están ben ou non.

### Solucións

#### Para os ficheiros truncados

1. Cambiar no orixinal o «;» por outro carácter (ou un símbolo) que após poidamos identificar facilmente.
2. Proceder a xerar o .po e comprobar que é correcto.
3. Volver deixar o orixinal como estaba e no .po deixar a cadea tal e como corresponde.
4. Continuar o traballo normalmente.

#### Para as cadeas truncadas

A única solución que atopei é a de corrixir esa(s) cadea(s) no(s) .php destino.

### Cambios no contido das cadeas

Por mor da propia estrutura do sistema de tradución en PHP, se nunha cadea, que xa estea traducida, á hora de facer os .po, non aparecerá marcada como «#fuzzy». Ás veces, este cambio é menor (corrección de typos, p. ex. screen por sceen), máis as veces os cambios poden seren máis importantes.

Un exemplo:

	#: $string['nativemysqlihelp']
	msgid ""
	"<p>The database is where most of the Moodle settings and data are stored and "
	"must be configured here.</p>\n"
	"<p>The database name, username, and password are required fields; table "
	"prefix is optional.</p>\n"
	"<p>If the database currently does not exist, and the user you specify has "
	"permission, Moodle will attempt to create a new database with the correct "
	"permissions and settings.</p>"
	msgstr ""
	"Agora necesita configurar a base de datos na que se almacenará a maioría dos "
	"datos de Moodle.\n"
	"A base de datos debe só poderá ser creada se o usuario da base de datos ten "
	"os permisos necesarios. Xa debe existir o nome de usuario e o contrasinal. O "
	"prefixo da táboa é opcional."

### Solución:

A única solución é a observación directa. Por iso é necesario facer sempre unha revisión cun chisco de atención.

## Script compLines

[Descargar](/scripts/compLines)

```
#!/bin/bash

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

# © Miguel Anxo Bouzada <mbouzada[fix@]gmail[fix.]com>
# © Proxecto Trasno <proxecto[fix@]trasno[fix.]net>
# april 30 2013

## -

## Asignación de variábeis de nomes de directorios de traballo
## Axuste estes nomes de cartafoles á súa comenencia

## Assigning directory names variables for  work
## Set these directory names for your convenience

PHP_Origin="en"
PHP_Target="gl"

[ -f stringDiff.txt ] && rm stringDiff.txt
[ -f listDiff.txt ] && rm listDiff.txt

ls ./${PHP_Target} > translated.txt

function countString(){
	# Compara o número de cadeas (concorrencia de $string[) en ficheiros
	# co mesmo nome en dous cartafoles diferentes (PHP_Origin e PHP_Target),
	# nun está o ficheiro orixinal e no outro o ficheiro xa traducido.

	# Ámbolos dous deben ter o mesmo número de «cadeas», de non ser así
	# envíanse os datos a un ficheiro (stringDiff.txt) coa lista.

	while read NAME
    do
		stringOrigin=`grep '\$string\[' ./${PHP_Origin}/${NAME} | wc -l`
echo ${stringOrigin}
		stringTarget=`grep '\$string\[' ./${PHP_Target}/${NAME} | wc -l`
echo ${stringTarget}
			if [ ${stringOrigin} != ${stringTarget} ]; then
				echo "${NAME} -- Original = ${stringOrigin} strings -- Translated = ${stringTarget} strings"  >> stringDiff.txt
			fi
    done < translated.txt
}

function countLines(){
	# Compara o número de liñas de ficheiros co mesmo nome en dous
	# cartafoles diferentes (PHP_Origin e PHP_Target), nun está o
	# ficheiro orixinal e no outro o ficheiro xa traducido.

	# Ámbolos dous deben ter o mesmo número de liñas, de non ser así
	# envíanse os datos a un ficheiro (listDiff.txt) coa lista.
      
	while read NAME
    do
		linOrigin=`wc -l ./${PHP_Origin}/${NAME} | cut -d" " -f1`
		linTarget=`wc -l ./${PHP_Target}/${NAME} | cut -d" " -f1`
			if [ ${linOrigin} != ${linTarget} ]; then
				echo "${NAME} -- Original = ${linOrigin} lines -- Translated = ${linTarget} lines"  >> listDiff.txt
			fi
    done < translated.txt
}

countString
countLines

#.EOF
```

## Terminoloxía e estilo

Estarase ao indicado nos ficheiros do cartafol anexo de Terminoloxía

### Construción sinxela dun ficheiro de terminoloxía «TBX», a partires dunha folla en «calc»

#### Exemplo con odstotbx 

Debemos partir dun ficheiro en Calc que conteña dúas columnas, unha para cada idioma. A primeira fila debe conter o identificador do idioma correspondente. 

| EN-US | GL
| --- | --- |
| SQL statement | Posición SQL |
| Application | Aplicativo |
| Not supplied | Non fornecido |

### Instalación 

* Descargar o XML Filter Package ODStoTBX.jar1 e gardalo localmente no seu equipo 
* Iniciar Calc de LibreOffice (ou de OpenOffice) 
* Seleccionar Ferramentas → Configuración de filtros XML... 
* Premer no botón Abrir un paquete... 
* Buscar o ficheiro ODStoTBX.jar no explorador de ficheiros e seleccionalo 
* Aparecerá un novo filtro chamado TermBase eXchange na lista dos filtros dispoñíbeis 
* Pechar o cadro de diálogo Configuración de filtros XML 

### Exportar

* Abrir o ficheiro Calc que conterá dúas columnas 
* Seleccionar Ficheiro → Exportar... 
* Seleccionar TBX - Termbase eXchange da caixa de opcións despregábel que aparece o pé.
* Activar o nome de ficheiro con extensión automática e gardar o seu ficheiro 

### Notas sobre o ficheiro resultante

Pode ser necesario cambiar a cabeceira do ficheiro resultante xa que ODStoTBX fornece unha cabeceira moi básica que pode dar problemas. 

### Cambiar a cabeceira do TBX

Pode ser necesario editar manualmente a cabeceira do ficheiro TBX xa que a veces a cabeceira é demasiado básica e pode dar problemas coa importación en determinadas ferramentas, por exemplo Terminator, ou ao pasarlle a ferramenta TBXcheck2 para comprobar que o esquema do TBX é válido. Pode empregarse a seguinte cabeceira como exemplo:

```
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE martif SYSTEM "TBXcoreStructV02.dtd">
<martif type="TBX" xml:lang="en">
  <martifHeader>
    <fileDesc>
      <titleStmt>
        <title>Terminoloxía de estatística de termos empregados en Moodle</title>
      </titleStmt>
      <sourceDesc>
        <p>Creado por Antón Méixome, 2011 con http://www.qa-distiller.com/files/ODStoTBX.jar
           Cabeceira modificada á man para poder validar empregando TBXcheck.</p>
      </sourceDesc>
    </fileDesc>
    <encodingDesc>
      <p type="XCSURI">http://www.gala-global.org/oscarStandards/tbx/TBXXCSV02.xcs</p>
    </encodingDesc>
  </martifHeader>
  <text>
```

## Bibliotecas útiles

Forman parte, tamén, do paquete translate-toolkit, xeralmente presente nos repositorios oficiais da maioría das distribucións.

po2csv – csv2po
csv2tbx
